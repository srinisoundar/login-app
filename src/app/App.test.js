import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';
import { Route } from 'react-router-dom';

import App from './App';
import {Login} from "./login";
import {AuthenticatedRoute} from "./utils/AuthenticatedRoute";
import {Home} from "./home";
import {PageNotFound} from "../components/PageNotFound";

describe('App Component',()=> {
  const initialState = {
    app: {
      loading: false,
      error: ''
    },
    login: {
      title: 'login'
    }
  };
  let pathMap = {};
  const mockStore = configureStore();
  let store, wrapper;
  let component;

  beforeEach(() => {
    store = mockStore(initialState);
    wrapper = mount(<Provider store={store}><App/></Provider>);
    component = shallow(<App/>);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(wrapper, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should show Login component for `/login` path', () => {
    pathMap = component.find(Route).reduce((pathMap, route) => {
      const routeProps = route.props();
      pathMap[routeProps.path] = routeProps.component;
      return pathMap;
    }, {});
    expect(pathMap['/login']).toBe(Login);
  });

  it('should show PageNotFound component for `undefined` path', () => {
    pathMap = component.find(Route).reduce((pathMap, route) => {
      const routeProps = route.props();
      pathMap[routeProps.path] = routeProps.component;
      return pathMap;
    }, {});
    expect(pathMap['undefined']).toBe(PageNotFound);
  });

  it('should show Home component for `/` & `/home` path', () => {
    pathMap = component.find(AuthenticatedRoute).reduce((pathMap, route) => {
      const routeProps = route.props();
      pathMap[routeProps.path] = routeProps.component;
      return pathMap;
    }, {});
    expect(pathMap['/']).toBe(Home);
    expect(pathMap['/home']).toBe(Home);
  });
});
