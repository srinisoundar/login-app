import Creators from './actions';
import {history} from "../utils/history";

const {
    requestLogin,
    responseLogin,
    errorLogin,
    requestLogout
} = Creators;

const clearUser = () => {
    localStorage.removeItem('user');
};

const handleResponse = (response) => {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                clearUser();
                window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
};

const login = url => (dispatch) => {
    dispatch(requestLogin(url));
    return fetch(url)
        .then(handleResponse)
        .then((user) => {
            localStorage.setItem('user', JSON.stringify(user));
            history.push('/');
            dispatch(responseLogin(user));
            return user;
            }, (error) => {
                dispatch(errorLogin(error.toString()));
            });
};

const logout = () => (dispatch) => {
    clearUser();
    dispatch(requestLogout());
};

export default {
    login,
    logout
};
