import types from './types';

const requestLogin = url => ({
  type: types.LOGIN_REQUEST,
  url
});

const responseLogin = user => ({
  type: types.LOGIN_RESPONSE,
  user
});

const errorLogin = error => ({
  type: types.LOGIN_ERROR,
  error
});

const requestLogout = () => ({
  type: types.REQUEST_LOGOUT
});


export default {
  requestLogin,
  responseLogin,
  errorLogin,
  requestLogout
};
