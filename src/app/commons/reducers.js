import types from './types';

const INITIAL_STATE = {
    loading: false,
    error: ''
};

const appReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.SET_AUTHENTICATION: {
            const {auth} = action;
            return {
                ...state,
                auth
            }
        }
        case types.LOGIN_REQUEST: {
            const {url} = action;
            return {
                ...state,
                url,
                loading: true
            };
        }

        case types.LOGIN_RESPONSE: {
            return {
                ...state,
                loading: false,
                error: ''
            };
        }

        case types.LOGIN_ERROR: {
            const {error} = action;
            return {
                ...state,
                loading: false,
                error: error
            };
        }

        case types.REQUEST_LOGOUT: {
            return {
                ...state
            };
        }
        default:
            return state;
    }
};

export default appReducer;
