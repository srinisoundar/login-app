import { combineReducers } from 'redux';
import loginReducer from './login/commons';
import appReducer from './commons';

const rootReducer = combineReducers({
  login: loginReducer,
  app: appReducer,
});

export default rootReducer;
