import React from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import {history} from './utils/history';
import {Login} from './login';
import {Home} from './home';
import {AuthenticatedRoute} from "./utils/AuthenticatedRoute";
import {PageNotFound} from "../components/PageNotFound";


function App() {
    return (
        <Router history={history}>
            <div>
                <Switch>
                    <AuthenticatedRoute exact path='/' component={Home}/>
                    <AuthenticatedRoute path='/home' component={Home}/>
                    <Route path='/login' component={Login}/>
                    <Route component={PageNotFound}/>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
