import React from 'react';
import './styles/HomeComponent.css';
import {history} from "../utils/history";

function HomeComponent({logout}) {
    const handleLogout = () => {
      logout();
      history.push('/login');
    };
    return (
        <main>
            <header>
                <nav className="navbar navbar-light bg-dark">
                    <a className="navbar-brand" href="/">
                        Home
                    </a>
                    <button
                        className="btn btn-outline-success"
                        type="button"
                        onClick={handleLogout}>
                        Logout
                    </button>
                </nav>
            </header>
            <section className="container m-5">
                <h1>Dummy Home Page</h1>
            </section>
        </main>
    );
}

export default HomeComponent;
