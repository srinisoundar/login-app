import { connect } from 'react-redux';
import HomeComponent from './HomeComponent';
import { appOperations } from '../commons';

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    const logout = () => {
        dispatch(appOperations.logout());
    };

    return {logout};
};

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeComponent);

export default HomeContainer;
