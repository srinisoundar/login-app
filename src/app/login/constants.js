export const usernameDefaults = {
    maxLength: 25,
    placeholder: 'User name',
    type: 'text',
    containerClassName: 'form-group row'
};

export const passwordDefaults = {
    maxLength: 32,
    placeholder: 'Password',
    type: 'password',
    containerClassName: 'form-group row'
};
