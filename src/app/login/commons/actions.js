import types from './types';

const setTitle = title => ({
  type: types.SET_LOGIN_TITLE,
  title,
});

export default {
  setTitle
};
