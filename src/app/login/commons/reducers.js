import types from './types';

const INITIAL_STATE = {
  title: ''
};

const loginReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.SET_LOGIN_TITLE: {
      const { title } = action;
      return {
        ...state,
        title,
      };
    }
    default:
      return state;
  }
};

export default loginReducer;
