import Creators from './actions';

const {setTitle} = Creators;

export default {
  setTitle
};
