import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { render, fireEvent} from "@testing-library/react";

import LoginComponent from "../LoginComponent";

describe('Login Component',()=> {
    const props = {
        title: 'test',
        styleOptions: {
            username: {
                maxLength: 15,
                placeholder: 'Name'
            },
            password: {
                maxLength: 30,
                placeholder: 'Password',
                containerClassName: 'password-container'
            }
        },
        setTitle:  () => {}
    };
    let component;

    describe('Shallow tests', () => {
        beforeEach(() => {
            component = shallow(<LoginComponent {...props}/>);
        });

        it('expect component to be rendered', () => {
            const div = document.createElement('div');
            ReactDOM.render(component, div);
            ReactDOM.unmountComponentAtNode(div);
        });

        it('expect component to render title', () => {
            expect(component.contains(<h1 className="h3 mb-3 font-weight-normal text-center">test</h1>)).toBe(true);
            expect(component.find('h1').get(0).props.children).toBe('test')
        });

        it('expect component to render username field with passed props', () => {
            const username = component.find('#username');
            expect(username.prop('placeholder')).toBe('Name');
            expect(username.prop('maxLength')).toBe(15);
            expect(username.prop('required')).toBe(true);
            expect(username.prop('name')).toBe('username');
            expect(username.prop('type')).toBe('text');
        });

        it('expect component to render username field with passed props', () => {
            const username = component.find('#password');
            expect(username.prop('placeholder')).toBe('Password');
            expect(username.prop('maxLength')).toBe(30);
            expect(username.prop('required')).toBe(true);
            expect(username.prop('name')).toBe('password');
        });
    });

    describe('Rendering tests', () => {
        let container;
        const fakeEvent = { preventDefault: () => {} };
        beforeEach(() => {
            const component = render(<LoginComponent {...props}/>);
            container = component.container;
        });

        it('should update state on username input change', () => {
            const username = container.querySelector('#username');
            const submit = container.querySelector('.login-form');
            fireEvent.change(username, { target: { value: 'Srini', name: 'username' } });
            fireEvent.submit(submit, fakeEvent);
            expect(username.value).toEqual('Srini');
        });

        it('should update state on password input change', () => {
            const password = container.querySelector('#password');
            const submit = container.querySelector('.login-form');
            fireEvent.change(password, { target: { value: 'abcabc', name: 'password' } });
            fireEvent.submit(submit, fakeEvent);
            expect(password.value).toEqual('abcabc');
        });

        it('expect login on valid form submit', () => {
            props.login = jest.fn();
            const {container} = render(<LoginComponent {...props}/>);
            const username = container.querySelector('#username');
            fireEvent.change(username, { target: { value: 'Srini', name: 'username' } });
            const password = container.querySelector('#password');
            const submit = container.querySelector('.login-form');
            fireEvent.change(password, { target: { value: 'abcabc', name: 'password' } });
            fireEvent.submit(submit, fakeEvent);
            expect(props.login.mock.calls.length).toBe(1);
        });

        it('expect error to be shown on error', () => {
            props.error = 'dummy error';
            const {container} = render(<LoginComponent {...props}/>);
            const error = container.querySelector ('.alert');
            expect(error.innerHTML).toEqual('dummy error');
        });

        describe('Password field validation tests', () =>{
            it('should empty password on invalid password', () => {
                const password = container.querySelector('#password');
                const submit = container.querySelector('.login-form');
                fireEvent.change(password, { target: { value: 'test123', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(password.value).toEqual('');
            });

            it('expect validation error on empty password', () => {
                const password = container.querySelector('#password');
                const submit = container.querySelector('.login-form');
                const msg = container.querySelector('.password-container .invalid-feedback');
                fireEvent.change(password, { target: { value: '', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(password.value).toEqual('');
                expect(msg.innerHTML).toEqual("Password can't be empty");
            });

            it('expect validation error on numbers and uppercase alphabets password', () => {
                const password = container.querySelector('#password');
                const submit = container.querySelector('.login-form');
                const msg = container.querySelector('.password-container .invalid-feedback');
                fireEvent.change(password, { target: { value: '1234', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password should contain only lowercase letters");
                fireEvent.change(password, { target: { value: 'Test', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password should contain only lowercase letters");
                fireEvent.change(password, { target: { value: 'Test123', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password should contain only lowercase letters");
            });

            it('expect password to not contain letters l,i in password', () => {
                const password = container.querySelector('#password');
                const submit = container.querySelector('.login-form');
                const msg = container.querySelector('.password-container .invalid-feedback');
                fireEvent.change(password, { target: { value: 'abcl', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password can't contain letters l, i");
                fireEvent.change(password, { target: { value: 'iabc', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password can't contain letters l, i");
            });

            it('expect password to contain one increasing straight 3 letters', () => {
                const password = container.querySelector('#password');
                const submit = container.querySelector('.login-form');
                const msg = container.querySelector('.password-container .invalid-feedback');
                fireEvent.change(password, { target: { value: 'ababab', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password must include one increasing straight of at least three letters like abc, bcd to xyz");
                fireEvent.change(password, { target: { value: 'acddee', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password must include one increasing straight of at least three letters like abc, bcd to xyz");
            });

            it('expect password to contain at least two non overlapping pairs of letters', () => {
                const password = container.querySelector('#password');
                const submit = container.querySelector('.login-form');
                const msg = container.querySelector('.password-container .invalid-feedback');
                fireEvent.change(password, { target: { value: 'abcde', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password must contain at least two non overlapping pairs of letters like aa, bb");
                fireEvent.change(password, { target: { value: 'xyzaa', name: 'password' } });
                fireEvent.submit(submit, fakeEvent);
                expect(msg.innerHTML).toEqual("Password must contain at least two non overlapping pairs of letters like aa, bb");
            });
        });
    });
});
