import { connect } from 'react-redux';
import LoginComponent from './LoginComponent';
import { appOperations } from '../commons';
import {loginOperations} from "./commons";

const mapStateToProps = (state) => {
    const { title } = state.login;
    const { loading, error } = state.app;
    return {
        loading,
        title,
        error
    };
};

const mapDispatchToProps = (dispatch) => {
    const login = (url) => {
        dispatch(appOperations.login(url));
    };

    const setTitle = (title) => {
        dispatch(loginOperations.setTitle(title));
    };

    return { login, setTitle};
};

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginComponent);

export default LoginContainer;
