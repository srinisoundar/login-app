import React, {useState, useEffect} from 'react';
import './styles/LoginComponent.css';
import {passwordDefaults, usernameDefaults} from "./constants";
import {config} from "../../config";
import SpinnerComponent from "../../components/SpinnerComponent";

function LoginComponent({styleOptions = {}, login, loading, setTitle, title, error} = {}) {
    let usernameOptions = usernameDefaults;
    let passwordOptions = passwordDefaults;
    let initialFormState = {
        username: '',
        password: ''
    };

    let [form, setFormValues] = useState(initialFormState);
    let [submit, setSubmit] = useState(false);
    let [invalidPwd, setInvalidPwd] = useState('');

    if (styleOptions.username) {
        usernameOptions = Object.assign(usernameOptions, styleOptions.username);
    }
    if (styleOptions.password) {
        passwordOptions = Object.assign(passwordOptions, styleOptions.password);
    }

    useEffect(() => {
        setTitle('Login');
    });

    const handleChange = (e) => {
        setFormValues({
            ...form,
            [e.target.name]: e.target.value
        });
    };

    const isNonOverlapping = (password) => {
        const map = {};
        let flag = false;
        for (let i = 0; i < password.length - 1; i++) {
            let j = i + 1;
            let pair = password[i] + password[j];
            if (map[pair]) {
                let t = map[pair];
                for (let k = 0; k < t.length; k++) {
                    let x = t[k].x;
                    let y = t[k].y;
                    if ((x !== i && x !== j) && (y !== i && y !== j)) {
                        flag = true;
                        break;
                    }
                }
            }
            if (!map[pair]) {
                map[pair] = [];
            }
            map[pair].push({x: i, y: j});
        }
        return flag;
    };

    const validatePassword = (password) => {
        if (!password) {
            setInvalidPwd('Password can\'t be empty');
            return false;
        }

        if (!/^[a-z]+$/.test(password)) {
            setInvalidPwd('Password should contain only lowercase letters');
            return false;
        }


        if (!/^[^li]+$/.test(password)) {
            setInvalidPwd('Password can\'t contain letters l, i');
            return false;
        }

        if (!/(abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz)+/.test(password)) {
            setInvalidPwd('Password must include one increasing straight of at least three letters like abc, bcd to xyz');
            return false;
        }

        if (!isNonOverlapping(password)) {
            setInvalidPwd('Password must contain at least two non overlapping pairs of letters like aa, bb');
            return false;
        }

        return true;
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        setSubmit(true);

        if (!validatePassword(form.password)) {
            setFormValues({
                ...form,
                password: ''
            });
            return;
        }

        if (form.username) {
            login(`${config.baseUrl}${config.loginUrl}`);
        }
    };

    return (
        <div className="container m-5">
            {error && <div className="col-6 m-auto text-center alert alert-danger" role="alert">
                {error}
            </div>}
            <form className="login-form" noValidate={true} onSubmit={handleSubmit}>
                <h1 className="h3 mb-3 font-weight-normal text-center">{title}</h1>
                <div className={usernameOptions.containerClassName}>
                    <label htmlFor="username" className="sr-only">User name</label>
                    <input type={usernameDefaults.type}
                           id="username"
                           name="username"
                           className={'form-control' + (submit && !form.username ? ' is-invalid' : '')}
                           placeholder={usernameOptions.placeholder}
                           required={true}
                           autoComplete="off"
                           autoFocus={true}
                           maxLength={usernameOptions.maxLength}
                           onChange={handleChange}
                           value={form.username}
                    />
                    <div className="invalid-feedback">
                        Please enter a username.
                    </div>
                </div>
                <div className={passwordOptions.containerClassName}>
                    <label htmlFor="password" className="sr-only">Password</label>
                    <input type={passwordOptions.type}
                           id="password"
                           name="password"
                           className={'form-control' + (submit && !form.password ? ' is-invalid' : '')}
                           placeholder={passwordOptions.placeholder}
                           required={true}
                           autoComplete="off"
                           maxLength={passwordOptions.maxLength}
                           onChange={handleChange}
                           value={form.password}
                    />
                    <div className="invalid-feedback text-break">
                        {invalidPwd}
                    </div>
                </div>
                <button className="btn btn-lg btn-primary btn-block" type="submit">
                    Login
                </button>
                <SpinnerComponent show={loading}></SpinnerComponent>
            </form>
        </div>
    );
}

export default LoginComponent;
