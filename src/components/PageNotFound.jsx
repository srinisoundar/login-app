import React from 'react';

export const PageNotFound = () => (
    <main className="jumbotron">
        <div className="container text-center">
            <h1>404 - Page not found</h1>
        </div>
    </main>
);
