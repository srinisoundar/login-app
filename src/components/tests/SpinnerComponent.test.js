import React from 'react';
import { render } from "@testing-library/react";
import SpinnerComponent from "../SpinnerComponent";



describe('Spinner Component',() => {
    it('expect to show spinner on true', () => {
        const {container} = render(<SpinnerComponent show={true}/>);
        expect(container.querySelector('.spinner-grow')).toBeDefined();
    });

    it('expect to hide spinner on false', () => {
        const {container} = render(<SpinnerComponent show={false}/>);
        expect(container.querySelector('.spinner-grow')).toBe(null);
    });
});
