import React from 'react';
import { render } from "@testing-library/react";
import {PageNotFound} from "../PageNotFound";



describe('Page Not found Component',() => {
    it('expect to render', () => {
        const {container} = render(<PageNotFound/>);
        expect(container.querySelector('.spinner-grow')).toBeDefined();
    });
});
