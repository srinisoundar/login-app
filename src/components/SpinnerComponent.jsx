import React from 'react';

function SpinnerComponent({ show }) {
	return (
		<div>
			{show && (
				<div className="d-flex justify-content-center m-5">
					<div className="spinner-grow" role="status"></div>
				</div>
			)}
		</div>
	);
}

export default SpinnerComponent;
