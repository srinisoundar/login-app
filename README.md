#Login-app
Login app was developed using react with redux. It contains a login page and a dummy home page. The page is authenticated with mock a login service.
The session is stored in localstorage to maintain across different tabs and cleared on logout from home page. 

The password comply to the following requirements
   
   * Passwords must include one increasing straight of at least three letters, like abc , cde , fgh ,
and so on, up to xyz . They cannot skip letters; acd doesn't count.
   * Passwords may not contain the letters i, O, or l, as these letters can be mistaken for other characters
and are therefore confusing.
   * Passwords must contain at least two non-overlapping pairs of letters, like aa, bb, or cc.
   * Passwords cannot be longer than 32 characters.
   * Passwords can only contain lower case alphabetic characters.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

##Description

The app follows duck style similar to domain style structure.  The app folder is the root module and consists of  each sub module. components folder contains all the reusable components.

Each module contains a `commons` folder which has all the redux stuff. The app has a reducers.js file which combines all the store into a global store from different modules.
All the styles goes inside the styles folder of each module. 

The AuthenticatedRoute takes care of redirecting the page to private pages like home to the authenticated users else redirects to login page.  

Each component is wrapped by a container which takes care of the store logic.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

